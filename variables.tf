variable "my_keypair_organization" {
  type = string
}

variable "my_keypair_environment" {
  type = string
}

variable "my_keypair_domain" {
  type = string
}

variable "my_keypair_create" {
  type        = string
  description = "Whether to create the AWS key pair(s) (which actually is just the public key, not the pair)"
}

variable "my_keypair_configs" {
  type        = any
  description = "A list (of maps) with the configurations for the AWS key pairs. The lenght of this list is used to determine how many AWS key pairs to create. The keys in each map should be key_name and public_key. Notice the organization, environment, domain and workspace get prepended to the key_name."
}
