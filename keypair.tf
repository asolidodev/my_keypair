resource "aws_key_pair" "my_keypair" {
  count      = var.my_keypair_create ? length(var.my_keypair_configs) : 0
  # Beware the key name syntax is used elsewhere (e.g. bastion) to get the resources
  # so if you change this naming structure you need to change there also
  key_name   = "${var.my_keypair_organization}_${var.my_keypair_environment}_${var.my_keypair_domain}_${terraform.workspace}_${lookup(var.my_keypair_configs[count.index], "key_name")}"
  public_key = lookup(var.my_keypair_configs[count.index], "public_key")
}
output "key_name" {
  value = aws_key_pair.my_keypair.*.key_name
}
